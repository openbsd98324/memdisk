# memdisk



## Boot FreeDOS

Use the GRUB of NetBSD 7.1.1., installed 2.04, on it msdos1, by using memdisk:


*Image Floppy:*
 
set root=(hd0,msdos1)

linux16 /root/freedos/memdisk/memdisk raw 

initrd16 /root/freedos/fdos1440.img



*Image CDROM:*
 
set root=(hd0,msdos1)

linux16 /root/freedos/memdisk/memdisk iso 

initrd16 /root/freedos/fdbasecd.iso

Source code: https://gitlab.com/openbsd98324/memdisk
